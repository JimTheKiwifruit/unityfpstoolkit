# Unity FPS Toolkit

A work in progress toolkit for creating simple FPS games using the Unity engine. Documentation and example scene require more work to showcase the features of this toolkit.

## Dependancies

- [UniRX 6.2.2](https://github.com/neuecc/UniRx/releases/tag/6.2.2)

## Setup

1. Download and install UniRX into your project.
2. Clone this repo and copy the 'Assets/FPSToolkit' folder to your own project. Optionally use this Unity project as a base.
3. Drop 'FPSToolkit/Prefabs/FPSPlayer.prefab' into a scene.
4. You may need to setup a 'Reload' input in the Input Manager.
5. Have fun :)

## TODO

- Complete example
- UI elements example
