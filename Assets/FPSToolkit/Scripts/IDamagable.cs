﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GimbalLock {

	/// <summary>
	/// Implement to receive Damage calls from WeaponShooter.
	/// </summary>
	public interface IDamagable {

		void Damage(DamageInfo info);

	}

}
