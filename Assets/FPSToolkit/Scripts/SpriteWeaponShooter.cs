﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;

namespace GimbalLock {

	public class SpriteWeaponShooter : WeaponShooter {

		[Header("Sprite")]
		new public MeshRenderer renderer;
		public Material idleMaterial;
		public Material fireMaterial;
		public float fireHoldTime = 0.2f;

		float beenHoldingFire = 0f;
		bool isFireing = false;

		protected override void Fire() {
			base.Fire();

			isFireing = true;
			SetSprite(fireMaterial);
		}

		void SetSprite(Material material) {
			renderer.material = material;
		}

		protected override void Update() {
			base.Update();

			if (isFireing) {
				if (beenHoldingFire < fireHoldTime) {
					beenHoldingFire += Time.deltaTime;
				} else {
					SetSprite(idleMaterial);
					beenHoldingFire = 0;
					isFireing = false;
				}
			}
		}

	}

}
