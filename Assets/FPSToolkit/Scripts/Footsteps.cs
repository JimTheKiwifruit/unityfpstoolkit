﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

namespace GimbalLock {

	[RequireComponent(typeof(RandomAudio))]
	public class Footsteps : MonoBehaviour {

		public float stepDistance = 1.7f;
		public CharacterController characterController;

		float currentDistance = 0;
		Vector3 lastPos = Vector3.zero;
		new AudioSource audio;
		RandomAudio randomAudio;
		AudioClip[] startAudioClips;
		ReactiveProperty<FootstepSurface> currentSurface = new UniRx.ReactiveProperty<FootstepSurface>(null);

		void Awake() {
			lastPos = transform.position;
			audio = GetComponent<AudioSource>();
			randomAudio = GetComponent<RandomAudio>();

			startAudioClips = randomAudio.clips;
			currentSurface.Subscribe(SetCurrentSurface);
		}

		void Update() {
			Vector3 fwd = transform.position - lastPos;
			if ((characterController && characterController.isGrounded) || !characterController)
				currentDistance += fwd.magnitude;
			if (currentDistance > stepDistance)
				Step();

			if (fwd == Vector3.zero)
				currentDistance = stepDistance;

			// get surface
			if (characterController) {
				FootstepSurface selectedSurface = null;
				foreach (var item in Physics.RaycastAll(characterController.transform.position, Vector3.down, (characterController.height / 2f) + 0.2f)) {
					var itemSurface = item.collider.GetComponent<FootstepSurface>();
					if (itemSurface != null)
						selectedSurface = itemSurface;
				}

				currentSurface.Value = selectedSurface;
			}

			lastPos = transform.position;
		}

		void SetCurrentSurface(FootstepSurface surface) {
			if (surface != null) {
				randomAudio.clips = surface.clips;
			} else {
				randomAudio.clips = startAudioClips;
			}
		}

		void Step() {
			currentDistance = 0;
			randomAudio.Play();
		}

	}

}
