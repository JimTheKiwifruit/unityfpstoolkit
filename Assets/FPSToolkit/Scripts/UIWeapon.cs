﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UnityEngine.UI;

namespace GimbalLock {

	public class UIWeapon : MonoBehaviour {

		public WeaponShooter shooter;
		public Text currentClipText;
		public Text currentAmmoText;

		private void Start() {
			shooter.CurrentClip.SubscribeToText(currentClipText);
			shooter.ammo.SubscribeToText(currentAmmoText);
		}

	} 

}
