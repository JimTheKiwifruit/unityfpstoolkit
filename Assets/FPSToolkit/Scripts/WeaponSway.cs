﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GimbalLock {

	public class WeaponSway : MonoBehaviour {

		public float translationSpeed = 2f;
		public float translationScale = 0.4f;
		public float rotationSpeed = 2f;
		public float rotationScale = 1.6f;

		Quaternion startLocalRot;
		Vector3 fwdLastFrame;
		Vector3 startLocalPos;
		Vector3 posLastFrame;

		private void Awake() {
			startLocalRot = transform.localRotation;
			startLocalPos = transform.localPosition;
		}

		private void LateUpdate() {
			var fromTo = Quaternion.FromToRotation(fwdLastFrame, Camera.main.transform.forward);
			fromTo = Quaternion.SlerpUnclamped(Quaternion.identity, fromTo, rotationScale);
			transform.localRotation = Quaternion.Lerp(transform.localRotation, startLocalRot * fromTo, Time.deltaTime * rotationSpeed);

			var dif = transform.InverseTransformDirection(posLastFrame - Camera.main.transform.position) * translationScale;
			transform.localPosition = Vector3.Lerp(transform.localPosition, startLocalPos + dif, Time.deltaTime * translationSpeed);

			fwdLastFrame = Camera.main.transform.forward;
			posLastFrame = Camera.main.transform.position;
		}

	}

}
