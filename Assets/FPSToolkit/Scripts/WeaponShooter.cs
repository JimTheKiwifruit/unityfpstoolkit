﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UniRx;

namespace GimbalLock {

	/// <summary>
	/// Base class for creating projectile weapons for both player and NPC.
	/// </summary>
	[RequireComponent(typeof(RandomAudio))]
	public class WeaponShooter : MonoBehaviour {

		public enum ProjectileType { Physics, Raycast };

		public int damageAmount = 1;
		public IntReactiveProperty ammo = new IntReactiveProperty(20);
		public int clipSize = 8;
		public float shotCooldown = 0.2f;
		public bool canReload = true;
		public float reloadTime = 2f;
		public bool autoReload = false;
		public float range = 100;
		public float force = 40;
		public float spread = 0.06f;
		public float aimSpread = 0.02f;
		public LayerMask layerMask = -1;
		public Transform source;
		public GameObject muzzelFlash;
		public GameObject hitEffect;
		public AudioClip dryFireClip;

		[Header("Projectile")]
		public ProjectileType projectileType = ProjectileType.Raycast;
		public Projectile projectilePrefab;

		protected RandomAudio randomAudio;
		[SerializeField]
		[Header("Protected")]
		protected IntReactiveProperty currentClip = new IntReactiveProperty(0);
		public IntReactiveProperty CurrentClip {
			get {
				return currentClip;
			}
		}
		protected bool reloading = false;
		protected bool aiming = false;
		protected ObjectPool physicsProjectilePool;
		protected ObjectPool hitEffectPool;

		// observables
		Subject<WeaponShooter> onFire = new Subject<WeaponShooter>();
		public IObservable<WeaponShooter> OnFire {
			get {
				return onFire.AsObservable();
			}
		}
		Subject<WeaponShooter> onReload = new Subject<WeaponShooter>();
		public IObservable<WeaponShooter> OnReload {
			get {
				return onReload.AsObservable();
			}
		}

		protected float currentShotCooldown = 0f;

		protected virtual void Awake() {
			randomAudio = GetComponent<RandomAudio>();
			if (muzzelFlash)
				muzzelFlash.SetActive(false);

			if (projectileType == ProjectileType.Physics) {
				physicsProjectilePool = ObjectPool.Create(projectilePrefab.gameObject, 20, true, false);
			}

			hitEffectPool = ObjectPool.Create(hitEffect, 10, true, true);
		}

		protected virtual void Update() {
			currentShotCooldown -= Time.deltaTime;
			if (currentShotCooldown < 0)
				currentShotCooldown = 0;
		}

		private void OnDestroy() {
			if (physicsProjectilePool)
				physicsProjectilePool.DestroyPool();

			onFire.OnCompleted();
			onReload.OnCompleted();
		}

		/// <summary>
		/// Attempt to fire weapon. Will call Fire() if conditions are successful.
		/// </summary>
		public virtual void TryFire() {
			if (currentClip.Value <= 0) {
				if (dryFireClip)
					randomAudio.PlayOneShot(dryFireClip);

				return;
			}

			if (reloading || currentShotCooldown > 0)
				return;

			Fire();
		}

		/// <summary>
		/// Called when weapon can be fired.
		/// </summary>
		protected virtual void Fire() {
			currentClip.Value--;

			Vector3 dir = CalcFireDirection();

			if (projectileType == ProjectileType.Physics) {
				physicsProjectilePool.Spawn(transform.position, transform.rotation)
					.GetComponent<Projectile>().Launch(this);
			} else if (projectileType == ProjectileType.Raycast) {
				RaycastHit hit;
				if (Physics.Raycast(transform.position, dir, out hit, range, layerMask, QueryTriggerInteraction.Ignore)) {
					var info = new DamageInfo(damageAmount, source ?? transform, hit.point, hit.normal, hit.collider);
					foreach (var dmg in hit.collider.GetComponentsInParent<IDamagable>())
						dmg.Damage(info);

					var rigid = hit.collider.GetComponent<Rigidbody>();
					if (rigid)
						rigid.AddForceAtPosition(transform.forward * force, hit.point, ForceMode.Force);

					if (hitEffectPool)
						hitEffectPool.Spawn(hit.point, Quaternion.LookRotation(hit.normal));
				}
			}

			currentShotCooldown = shotCooldown;

			randomAudio.Play();

			if (muzzelFlash) {
				muzzelFlash.SetActive(true);
				muzzelFlash.transform.Rotate(0, 0, Random.value * 360f, Space.Self);
				Observable.TimerFrame(2).Subscribe(_ => muzzelFlash.SetActive(false));
			}

			onFire.OnNext(this);

			if (currentClip.Value == 0 && autoReload)
				TryReload();
		}

		protected Vector3 CalcFireDirection() {
			return transform.forward + (Random.insideUnitSphere * (aiming ? aimSpread : spread));
		}

		/// <summary>
		/// Attempt to reload weapon. Will call Reload() if conditions are successful.
		/// </summary>
		public virtual void TryReload() {
			if (!canReload || reloading || ammo.Value <= 0)
				return;

			Reload();
		}

		/// <summary>
		/// Called when weapon can be reloaded.
		/// </summary>
		protected virtual void Reload() {
			onReload.OnNext(this);
			StartCoroutine(Reloading());
		}

		/// <summary>
		/// WIP: Iron-sights/aim-down-sights
		/// </summary>
		public virtual void Aim() {
			aiming = true;
		}

		public virtual void StopAim() {
			aiming = false;
		}

		IEnumerator Reloading() {
			reloading = true;

			yield return new WaitForSeconds(reloadTime);
			//var ammoCanAdd = Mathf.Min(ammo, clipSize);
			int currentClipLevel = Mathf.Min(clipSize - currentClip.Value, ammo.Value, clipSize);
			currentClip.Value += currentClipLevel;
			ammo.Value -= currentClipLevel;

			reloading = false;
		}

		private void OnDrawGizmos() {
			Gizmos.color = new Color(0, 1, 0, 0.5f);
			Gizmos.DrawLine(transform.position, transform.position + (transform.forward * 0.4f));
		}

	}

}
