﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GimbalLock {

	public class SurfaceID : MonoBehaviour, IDamagable {

		public GameObject bulletDecal;
		public float decalOffset = 0.006f;

		ObjectPool bulletDecalPool;

		private void Awake() {
			bulletDecalPool = ObjectPool.Create(bulletDecal, 20, false, true);
		}

		public void Damage(DamageInfo info) {
			if (bulletDecal) {
				var go = bulletDecalPool.Spawn(info.point + (info.normal * decalOffset), Quaternion.LookRotation(info.normal), info.collider.transform);
				go.transform.Rotate(0, 0, Random.value * 360f, Space.Self);
			}
		}

	}

}
