﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GimbalLock {

	public class DamageInfo {

		public int amount;
		public Transform source;
		public Vector3 point;
		public Vector3 normal;
		public Collider collider;

		public DamageInfo(int amount) {
			this.amount = amount;
		}

		public DamageInfo(int amount, Transform source) {
			this.amount = amount;
			this.source = source;
		}

		public DamageInfo(int amount, Transform source, Vector3 point, Vector3 normal, Collider collider) {
			this.amount = amount;
			this.source = source;
			this.point = point;
			this.normal = normal;
			this.collider = collider;
		}
	}

}
