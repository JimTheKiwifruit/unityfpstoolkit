﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

namespace GimbalLock {

	/// <summary>
	/// Basic camera z roll when moving.
	/// </summary>
	public class CameraMoveRoll : MonoBehaviour {

		public float magnitude = 10;
		public float speed = 10;
		public CharacterController character;

		Vector3 posLastFrame;

		private void LateUpdate() {
			var dir = posLastFrame - transform.position;
			var dirMag = dir.magnitude;

			if (character && !character.isGrounded)
				dirMag = 0;

			transform.localRotation = Quaternion.AngleAxis(Mathf.Sin(Time.time * speed) * dirMag * magnitude, Vector3.forward);

			posLastFrame = transform.position;
		}

	}

}
