﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GimbalLock {

	/// <summary>
	/// Unparent 'target' and redirect OnEnable, OnDisable and Destroy from attached GameObject.
	/// </summary>
	public class UnparentLink : MonoBehaviour {

		public Transform target;
		public bool onAwake = true;
		public bool unparentSnapToZero = false;

		Transform startParent;

		bool Unparented {
			get {
				if (!target)
					return false;

				return target.parent != startParent;
			}
		}

		private void Awake() {
			startParent = target.transform.parent;

			if (onAwake)
				Unparent();
		}

		public void Unparent() {
			if (!target)
				return;

			target.transform.SetParent(null);
			if (unparentSnapToZero)
				target.transform.position = Vector3.zero;
		}

		public void Reparent(bool worldPositionStays = true) {
			if (!target)
				return;

			target.transform.SetParent(startParent, worldPositionStays);
		}

		private void OnDestroy() {
			if (!target)
				return;

			if (Unparented)
				Destroy(target.gameObject);
		}

		private void OnEnable() {
			if (!target)
				return;

			if (Unparented)
				target.gameObject.SetActive(true);
		}

		private void OnDisable() {
			if (!target)
				return;

			if (Unparented)
				target.gameObject.SetActive(false);
		}

	}

}
