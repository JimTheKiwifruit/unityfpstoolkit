﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GimbalLock {

	public class ExplodeOnAwake : MonoBehaviour {

		public Vector3 relativeCenter;
		public float force = 50;
		public float radius = 4f;

		private void Awake() {
			foreach (var item in GetComponentsInChildren<Rigidbody>()) {
				item.AddExplosionForce(force, transform.position + relativeCenter, radius);
			}
		}

		private void OnDrawGizmosSelected() {
			Gizmos.color = new Color(1, 0, 0, 0.5f);
			Gizmos.DrawSphere(transform.position + relativeCenter, 0.07f);
		}

	}

}
