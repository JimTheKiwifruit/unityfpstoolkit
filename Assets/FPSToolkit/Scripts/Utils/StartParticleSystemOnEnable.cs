﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GimbalLock {

	[RequireComponent(typeof(ParticleSystem))]
	public class StartParticleSystemOnEnable : MonoBehaviour {

		new ParticleSystem particleSystem;

		private void Awake() {
			particleSystem = GetComponent<ParticleSystem>();
		}

		private void OnEnable() {
			particleSystem.Play();
		}

	}

}
