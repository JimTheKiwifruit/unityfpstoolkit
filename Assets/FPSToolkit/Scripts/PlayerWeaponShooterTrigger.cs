﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GimbalLock {

	public class PlayerWeaponShooterTrigger : MonoBehaviour {

		public WeaponShooter shooter;
		public bool semiAuto = false;

		private void Update() {
			if (!shooter)
				return;

			if (semiAuto) {
				if (Input.GetButtonDown("Fire1"))
					shooter.TryFire();
				else if (Input.GetButtonDown("Reload"))
					shooter.TryReload();
			} else {
				if (Input.GetButton("Fire1"))
					shooter.TryFire();
				else if (Input.GetButtonDown("Reload"))
					shooter.TryReload();
			}
		}

	}

}
