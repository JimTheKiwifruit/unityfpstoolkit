﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GimbalLock {

	/// <summary>
	/// Moves a character controller around the scene.
	/// </summary>
	[RequireComponent(typeof(CharacterController))]
	public class FPSMovement : MonoBehaviour {

		public float moveSpeed = 6f;
		public float jumpSpeed = 8f;
		public float airMovementScale = 0.6f;
		public float gravity = 20f;
		public float stickDistance = 0.15f;

		Vector3 dir = Vector3.zero;
		CharacterController controller;
		bool isJumping = false;

		void Start() {
			controller = GetComponent<CharacterController>();
			transform.position += Vector3.up * (controller.height / 2f);
		}

		void Update() {
			var input = Vector3.ClampMagnitude(new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical")), 1f);
			input = transform.TransformDirection(input);

			Vector3 airDir = Vector3.zero;

			if (controller.isGrounded) {
				dir = input * moveSpeed;

				isJumping = false;

				if (Input.GetButtonDown("Jump")) {
					dir.y = jumpSpeed;
					isJumping = true;
				}
			} else {
				dir.y -= gravity * Time.deltaTime;

				airDir = input * moveSpeed * airMovementScale;
			}

			controller.Move((dir + airDir) * Time.deltaTime);
		}

		private void LateUpdate() {
			if (!controller.isGrounded && !isJumping) {
				Ray stickRay = new Ray(transform.position + (Vector3.down * controller.height * 0.5f), Vector3.down);
				Debug.DrawRay(stickRay.origin, stickRay.direction * stickDistance, Color.red);
				RaycastHit hit;
				if (Physics.Raycast(stickRay, out hit, stickDistance)) {
					controller.Move(Vector3.down * hit.distance);
				}
			}
		}
	}

}
