﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GimbalLock {

	public class UIHitFeedback : MonoBehaviour {

		public Image topImage;
		public Image bottomImage;
		public Image leftImage;
		public Image rightImage;
		public float fadeTime = 1.5f;
		[Range(0f, 1f)]
		public float brightness = 1f;

		private void Start() {
			EachImage(i => SetAlpha(i, 0f));
		}

		public void Hit(Vector3 sourcePos) {
			var cam = Camera.main;
			var dir = cam.transform.InverseTransformDirection(sourcePos - cam.transform.position);
			var screenDir = new Vector2(dir.x, dir.z).normalized;

			SetAlpha(topImage, screenDir.y);
			SetAlpha(bottomImage, -screenDir.y);
			SetAlpha(leftImage, -screenDir.x);
			SetAlpha(rightImage, screenDir.x);
		}

		private void Update() {
			EachImage(i => i.color = new Color(i.color.r, i.color.g, i.color.b, Mathf.Max(i.color.a - Time.deltaTime * fadeTime, 0f)));
		}

		void SetAlpha(Image image, float val) {
			image.color = new Color(image.color.r, image.color.g, image.color.b, val * brightness);
		}

		void EachImage(System.Action<Image> action) {
			action(topImage);
			action(bottomImage);
			action(leftImage);
			action(rightImage);
		}

	}

}
