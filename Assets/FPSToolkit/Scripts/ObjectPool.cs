﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UniRx;

namespace GimbalLock {

	public class ObjectPool : MonoBehaviour {

		public GameObject prefab;
		public int maxSize = 1;
		public bool autoResize = true;

		List<GameObject> pool;
		List<GameObject> spawnedPool;
		int nonResizeIndex = 0;
		static Dictionary<GameObject, ObjectPool> sharedObjectPools = new Dictionary<GameObject, ObjectPool>();

		public static ObjectPool Create(GameObject prefab, int maxSize, bool autoResize, bool shared) {
			if (prefab == null)
				return null;

			if (shared && sharedObjectPools.ContainsKey(prefab))
				return sharedObjectPools[prefab];

			ObjectPool pool = new GameObject("ObjectPool(" + prefab.name + ")").AddComponent<ObjectPool>().Init(prefab, maxSize, autoResize);
			if (shared)
				sharedObjectPools.Add(prefab, pool);

			return pool;
		}

		public ObjectPool Init(GameObject prefab, int maxSize, bool autoResize) {
			this.prefab = prefab;
			this.maxSize = maxSize;
			this.autoResize = autoResize;

			pool = new List<GameObject>(maxSize);
			for (int i = 0; i < maxSize; i++) {
				AddInstanceToPool();
			}

			if (!autoResize)
				spawnedPool = new List<GameObject>(maxSize);

			return this;
		}

		GameObject AddInstanceToPool() {
			GameObject go = Instantiate(prefab, transform);
			go.SetActive(false);
			pool.Add(go);
			return go;
		}

		public void Clear() {
			pool.Clear();
			if (spawnedPool != null)
				spawnedPool.Clear();
		}

		public GameObject Spawn(Vector3 position, Quaternion rotation, Transform parent = null) {
			GameObject go = autoResize ? GetAutoResize() : GetNonAutoResize();

			go.SetActive(true);
			go.transform.SetParent(parent);
			go.transform.SetPositionAndRotation(position, rotation);

			return go;
		}

		GameObject GetItem() {
			GameObject go = null;
			foreach (var item in pool) {
				if (item == null)
					continue;

				if (!item.activeSelf) {
					go = item;
				}
			}

			return go;
		}

		GameObject GetAutoResize() {
			GameObject go = GetItem();

			if (go == null)
				go = AddInstanceToPool();

			return go;
		}

		GameObject GetNonAutoResize() {
			GameObject go = GetItem();

			if (go == null) {
				go = spawnedPool[nonResizeIndex];
				nonResizeIndex++;
				if (nonResizeIndex >= spawnedPool.Count) {
					nonResizeIndex = 0;
				}
			} else {
				spawnedPool.Remove(go);
				spawnedPool.Add(go);
			}

			return go;
		}

		public void DestroyPool() {
			if (gameObject)
				Destroy(gameObject);
		}

		private void OnDestroy() {
			pool.ForEach(Destroy);
			if (spawnedPool != null)
				spawnedPool.ForEach(Destroy);
		}

	}

}
