﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GimbalLock {

	/// <summary>
	/// Play a random AudioClip with random pitch from the 'clips' array. 
	/// Prevents the same clip from being played twice, even if destroyed provided a 'playOnAwakePersistKey' is provided.
	/// </summary>
	[RequireComponent(typeof(AudioSource))]
	public class RandomAudio : MonoBehaviour {

		static Dictionary<string, int> lastPlayedClips;

		public AudioClip[] clips;
		[Range(0, 3)]
		public float randomPitch = 0;
		public bool playOnAwake = true;
		[Tooltip("Provide a string if this audio source will be instanciated repeatidly.")]
		public string playOnAwakePersistKey;
		public bool detatchFromParent = false;
		public bool destroyAfter = false;

		AudioSource audioSource;
		float startPitch;
		int lastClipPlayed = -1;
		bool isDestroying = false;

		private void Awake() {
			audioSource = GetComponent<AudioSource>();
			startPitch = audioSource.pitch;

			if (playOnAwake) {
				if (string.IsNullOrEmpty(playOnAwakePersistKey))
					Play();
				else
					PlayPersistLastClip(playOnAwakePersistKey);
			}
		}

		public void PlayOneShot(AudioClip clip) {
			audioSource.PlayOneShot(clip);
		}

		/// <summary>
		/// Play a random clip.
		/// </summary>
		public void Play() {
			if (isDestroying || clips == null || clips.Length == 0)
				return;

			if (clips.Length == 1) {
				PlayClip(0);
				TryDestroy(clips[0].length);
			} else {
				var n = GetRandomClip(lastClipPlayed);
				PlayClip(n);
				TryDestroy(clips[n].length);
			}
		}

		int GetRandomClip(int lastClip = -1) {
			int n;
			do {
				n = Random.Range(0, clips.Length);
			} while (n == lastClip);
			return n;
		}

		void PlayClip(int clip) {
			if (detatchFromParent)
				transform.SetParent(null);

			audioSource.Stop();
			audioSource.clip = clips[clip];
			audioSource.pitch = startPitch + Random.Range(-randomPitch, randomPitch);
			audioSource.Play();

			lastClipPlayed = clip;
		}

		public void PlayPersistLastClip(string key) {
			if (lastPlayedClips == null)
				lastPlayedClips = new Dictionary<string, int>();

			int lastClip = lastPlayedClips.ContainsKey(key) ? lastPlayedClips[key] : -1;
			int n = GetRandomClip(lastClip);
			PlayClip(n);

			if (lastPlayedClips.ContainsKey(key))
				lastPlayedClips[key] = n;
			else
				lastPlayedClips.Add(key, n);

			TryDestroy(clips[n].length);
		}

		void TryDestroy(float length) {
			if (destroyAfter) {
				Destroy(gameObject, length);
				isDestroying = true;
			}
		}

	}

}
