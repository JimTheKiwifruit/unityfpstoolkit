﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UniRx;

namespace GimbalLock {

	/// <summary>
	/// Basic implementation of IDamagable that should be extendable for most kinds damage handling.
	/// </summary>
	public class DamageHandler : MonoBehaviour, IDamagable {

		public IntReactiveProperty health = new IntReactiveProperty(100);
		public IntReactiveProperty maxHealth = new IntReactiveProperty(100);
		public float destroyDelay = 0;
		public GameObject spawnOnDeath;

		public UnityEvent onDamage;
		public UnityEvent onDeath;

		public bool IsDead {
			get {
				return health.Value == 0;
			}
		}

		Animator animator;
		bool doneDeath = false;

		protected virtual void Awake() {
			animator = GetComponent<Animator>();
		}

		public virtual void Damage(DamageInfo damageInfo) {
			health.Value = Mathf.Clamp(health.Value - damageInfo.amount, 0, maxHealth.Value);

			if (health.Value <= 0 && !doneDeath) {
				DoDeath();
				doneDeath = true;
			} else {
				DoDamage(damageInfo);
			}
		}

		protected virtual void DoDamage(DamageInfo damageInfo) {
			onDamage.Invoke();
		}

		protected virtual void DoDeath() {
			if (GetComponent<Collider>())
				GetComponent<Collider>().enabled = false;

			onDeath.Invoke();

			if (spawnOnDeath)
				Instantiate(spawnOnDeath, transform.position, transform.rotation);

			Destroy(gameObject, destroyDelay);
		}

	}

}
