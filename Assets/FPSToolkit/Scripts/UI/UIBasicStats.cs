﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;
using UnityEngine.UI;

namespace GimbalLock {

	public class UIBasicStats : MonoBehaviour {

		public DamageHandler damageHandler;
		public WeaponShooter weaponShooter;
		public Text healthText;
		public Text ammoText;

		private void Start() {
			damageHandler.health.SubscribeToText(healthText);
			weaponShooter.CurrentClip.SubscribeToText(ammoText);
		}

	}

}
