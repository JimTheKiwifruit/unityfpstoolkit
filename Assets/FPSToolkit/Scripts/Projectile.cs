﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GimbalLock {

	[RequireComponent(typeof(Rigidbody), typeof(Collider))]
	public class Projectile : MonoBehaviour {

		public float force = 100;

		WeaponShooter source;
		Rigidbody rigid;

		private void Awake() {
			rigid = GetComponent<Rigidbody>();
		}

		public void Launch(WeaponShooter source) {
			this.source = source;

			rigid.velocity = Vector3.zero;
			rigid.AddForce(transform.forward * force, ForceMode.Impulse);
		}

		private void OnTriggerEnter(Collider other) {
			var info = new DamageInfo(source.damageAmount, source.source ?? transform);
			foreach (var dmg in other.GetComponentsInParent<IDamagable>())
				dmg.Damage(info);

			gameObject.SetActive(false);
		}

		private void OnCollisionEnter(Collision collision) {
			var rigid = collision.collider.GetComponent<Rigidbody>();
			if (rigid)
				rigid.AddForceAtPosition(this.rigid.velocity, collision.contacts[0].point, ForceMode.Force);

			OnTriggerEnter(collision.collider);
		}

	}

}
