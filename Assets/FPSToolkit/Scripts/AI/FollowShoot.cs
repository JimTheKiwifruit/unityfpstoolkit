﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;

namespace GimbalLock {

	[RequireComponent(typeof(CharacterController))]
	public class FollowShoot : MonoBehaviour {

		public WeaponShooter shooter;
		public float range = 8f;
		public float moveSpeed = 6f;
		public float rotateSpeed = 20f;

		Transform player;
		CharacterController controller;

		private void Awake() {
			player = GameObject.FindWithTag("Player").transform;
			controller = GetComponent<CharacterController>();

			Observable.Interval(System.TimeSpan.FromSeconds(0.5f)).Subscribe(_ => {
				if (this.enabled)
					shooter.TryFire();
			}).AddTo(this);
		}

		private void Update() {
			transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(Vector3.ProjectOnPlane(player.position - transform.position, Vector3.up)), rotateSpeed * Time.deltaTime);

			if (Vector3.Distance(transform.position, player.position) > range) {
				controller.SimpleMove(transform.forward * moveSpeed);
			}
		}

	}

}
