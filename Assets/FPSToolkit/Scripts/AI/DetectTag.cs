﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

namespace GimbalLock {

	public class DetectTag : MonoBehaviour {

		new public string tag = "Player";
		public float range = 10f;
		public bool repeat = false;
		public AudioClip audioOnDetect;
		public MonoBehaviour next;
		//public UnityEvent onDetect;

		GameObject taggedObject;
		AudioSource audioSource;

		private void Awake() {
			taggedObject = GameObject.FindWithTag(tag);

			audioSource = GetComponentInChildren<AudioSource>();
		}

		private void Update() {
			if (Vector3.Distance(transform.position, taggedObject.transform.position) < range) {
				if (audioSource && audioOnDetect)
					audioSource.PlayOneShot(audioOnDetect);

				Next();
				this.enabled = false;
			}
		}

		void Next() {
			//onDetect.Invoke();
			next.enabled = true;
			this.enabled = false;
		}

		private void OnDrawGizmosSelected() {
			Gizmos.color = Color.green;
			Gizmos.DrawWireSphere(transform.position, range);
		}

	}

}
