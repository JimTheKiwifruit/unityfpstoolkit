﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GimbalLock {

	/// <summary>
	/// Basic mouse look with vertical angle clamping. 
	/// Y rotation is applied to 'characterControllerObject' when supplied.
	/// </summary>
	public class FPSCameraLook : MonoBehaviour {

		public Transform characterControllerObject;
		public float clampAngle = 80;
		public float sensitivity = 3;

		float rotX;
		float rotY;

		private void Start() {
			LockCursor(true);
		}

		void Update() {
			float mouseX = Input.GetAxis("Mouse X");
			float mouseY = -Input.GetAxis("Mouse Y");

			rotY += mouseX * sensitivity;
			rotX += mouseY * sensitivity;

			rotX = Mathf.Clamp(rotX, -clampAngle, clampAngle);

			if (characterControllerObject) {
				characterControllerObject.localRotation = Quaternion.Euler(0, rotY, 0);
				transform.localRotation = Quaternion.Euler(rotX, 0, 0);
			} else {
				transform.localRotation = Quaternion.Euler(rotX, rotY, 0);
			}
			
		}

		public static void LockCursor(bool lockCurser) {
			Cursor.lockState = lockCurser ? CursorLockMode.Locked : CursorLockMode.None;
			Cursor.visible = !lockCurser;
		}

	}

}
